package cn.zhxu.toys.util;

import org.junit.Assert;
import org.junit.Test;

public class CrcUtilsTests {

    @Test
    public void crc16_1() {
        byte[] data = {
                0x01, 0x03, 0x00, 0x09, 0x00, 0x01
        };
        byte[] res = CRCUtils.crc16b(data, 0, data.length);
        Assert.assertEquals("5408", StringUtils.toHexStr(res));
    }

    @Test
    public void crc16_2() {
        byte[] data = {
                0x01, 0x03, 0x02, 0x00, (byte)0xEE
        };
        byte[] res = CRCUtils.crc16b(data, 0, data.length);
        Assert.assertEquals("3808", StringUtils.toHexStr(res));
    }

}

package cn.zhxu.toys.oss;

import java.io.File;
import java.io.InputStream;

/**
 * 对象存储管理器
 * @author Troy
 */
public interface OssManager {

	/**
	 * 上传文件
	 * @param key 键
	 * @param file 文件
	 * @return 文件访问路径
	 */
	String upload(String key, File file);
	
	/**
	 * 上传文件
	 * @param key 键
	 * @param contentType 类型
	 * @param inputStream 输入流
	 * @return 文件访问路径
	 */
	String upload(String key, String contentType, InputStream inputStream);

	/**
	 * 上传文件(指定文件名)
	 * @param fileName 文件名
	 * @param key 键
	 * @param contentType 类型
	 * @param inputStream 输入流
	 * @return 文件访问路径
	 */
	String upload(String fileName, String key, String contentType, InputStream inputStream);

	/**
	 * 访问文件
	 * @param key 键
	 * @return 文件输入流
	 */
	InputStream access(String key);

	/**
	 * 删除文件
	 * @param fileUrl 文件 URL
	 * @return true if deleted
	 */
	boolean delete(String fileUrl);
	
}

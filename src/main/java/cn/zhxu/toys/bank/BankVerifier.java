package cn.zhxu.toys.bank;

/**
 * 银行卡验证器
 * 
 * @author Troy.Zhou
 * 
 */
public interface BankVerifier {

	/**
	 * 四要素认证
	 * 
	 * @param realName
	 *            真实姓名
	 * @param idCardNo
	 *            身份证号
	 * @param bankPhone
	 *            银行预留手机号
	 * @param bankCardNo
	 *            银行卡号
	 */
	Result verify(String realName, String idCardNo, String bankPhone, String bankCardNo);

	
	/**
	 * 四要素认证结果
	 */
	class Result {

		private boolean ok = true;
		private String message;

		public Result(boolean ok, String message) {
			super();
			this.ok = ok;
			this.message = message;
		}

		public boolean isOk() {
			return ok;
		}

		public void setOk(boolean ok) {
			this.ok = ok;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public static Result fail(String message) {
			return new Result(false, message);
		}
		
		public static Result ok() {
			return new Result(true, "ok");
		}
		
	}

}

package cn.zhxu.toys.tree;

public interface TreeAble {

    Integer getId();

    Integer getParentId();

}

package cn.zhxu.toys.tree;

import java.util.List;

public interface TreeOutput<R extends TreeOutput<R>> {

    void setChildren(List<R> children);

    List<R> getChildren();

}

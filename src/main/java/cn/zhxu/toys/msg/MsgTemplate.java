package cn.zhxu.toys.msg;


/**
 * 消息模板
 * 
 * @author Troy.Zhou
 *
 */
public class MsgTemplate {

	private String name;
	private String[] tmplArgs;
	private String tmplValue;

	public MsgTemplate() {
	}

	public MsgTemplate(String name, String tmplValue) {
		this.name = name;
		this.tmplValue = tmplValue;
	}
	
	public MsgTemplate(String name, String tmplValue, String[] tmplArgs) {
		this.name = name;
		this.tmplValue = tmplValue;
		this.tmplArgs = tmplArgs;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * use tmplValue instead.
	 * @see #getTmplValue()
	 */
	@Deprecated
	public String getTmplId() {
		return getTmplValue();
	}

	/**
	 * use tmplValue instead.
	 * @see #setTmplValue(String)
	 */
	@Deprecated
	public void setTmplId(String tmplId) {
		this.setTmplValue(tmplId);
	}

	public String[] getTmplArgs() {
		return tmplArgs;
	}

	public void setTmplArgs(String[] tmplArgs) {
		this.tmplArgs = tmplArgs;
	}

	public String getTmplValue() {
		return tmplValue;
	}

	public void setTmplValue(String tmplValue) {
		this.tmplValue = tmplValue;
	}

}

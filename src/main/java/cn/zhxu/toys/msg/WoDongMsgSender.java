package cn.zhxu.toys.msg;

import cn.zhxu.okhttps.HttpResult;
import cn.zhxu.okhttps.HttpUtils;
import cn.zhxu.data.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;


/**
 * 
 * @author Administrator
 *
 */
public class WoDongMsgSender extends AbstractMsgSender {

	static Logger log = LoggerFactory.getLogger(WoDongMsgSender.class);

	public static String DEFAULT_ENDPOINT = "https://client.movek.net:8443/sendvarsms.aspx";

	private String endpoint = DEFAULT_ENDPOINT;

	// 特服号
	private String userId;
	// 序列号
	private String account;

	private String password;

	@Override
	public void init(Map<String, String> params) throws MsgSenderInitException {
		String endpoint = params.get("endpoint");
		String userId = params.get("userId");
		String account = params.get("account");
		String password = params.get("password");
		if (userId == null) {
			throw new MsgSenderInitException("缺少参数：userId");
		}
		if (account == null) {
			throw new MsgSenderInitException("缺少参数：account");
		}
		if (password == null) {
			throw new MsgSenderInitException("缺少参数：password");
		}
		if (endpoint != null) {
			this.endpoint = endpoint;
		}
		this.userId = userId;
		this.account = account;
		this.password = password;
	}

	@Override
	public boolean send(String phone, MsgTemplate template, String... tmplArgs) {
		String content = template.getTmplValue();
		if (tmplArgs != null) {
			String[] argNames = template.getTmplArgs();
			if (tmplArgs.length > 0 && (argNames == null || argNames.length < tmplArgs.length)) {
				throw new RuntimeException("WoDongMsgTemplate tmplArgs not match: " + Arrays.toString(argNames) + " to " + Arrays.toString(tmplArgs));
			}
			for (int index = 0; index < tmplArgs.length; index ++) {
				String placeHolder = "${" + argNames[index] + "}";
				content = content.replace(placeHolder, tmplArgs[index]);
			}
		}
		try {
			content = URLEncoder.encode(content, "utf-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
		Map<String, String> params = new HashMap<String, String>();
		params.put("action", "send");
		params.put("userid", userId);
		params.put("account", account);
		params.put("password", password);
		params.put("mobile", phone);
		params.put("content", content);
		params.put("json", "1");
		try {
			return send(phone, params);
		} catch (Exception e) {
			throw new RuntimeException("聚合信息发送异常：", e);
		}
	}

	private boolean send(String phone, Map<String, String> params) throws Exception {
		log.info("send params [" + phone + "] = " + params);
		HttpResult result = HttpUtils.sync(endpoint).addBodyPara(params).post();
		if (!result.isSuccessful()) {
			log.error("沃动短信返回状态码错误：" + result.getStatus());
			return false;
		}
		Mapper json = result.getBody().toMapper();
		String code = json.getString("code");
		if (code != null && code.toLowerCase().equals("success")) {
			return true;
		} else {
			log.error("发送信息不成功： " + json);
			return false;
		}
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}

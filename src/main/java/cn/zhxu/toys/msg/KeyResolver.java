package cn.zhxu.toys.msg;

/**
 * @author Troy.Zhou @ 2022/8/17 21:02
 */
public interface KeyResolver {

	/**
	 * 获取当前请求者的标识
	 * @param phone 手机号
	 * @return ip of the current request
	 */
	String revolve(String phone);

}

package cn.zhxu.toys.msg;


/**
 * 
 * 信息发送接口
 * 
 * @author Troy.Zhou
 *
 */

public interface MsgSender {


	/**
	 * 发送信息
	 * @param phone 手机号
	 * @param tmplName 模板名称
	 * @param tmplArgs 模板参数值
	 * @return 是否发送成功
	 */
	boolean send(String phone, String tmplName, String... tmplArgs);
	
}

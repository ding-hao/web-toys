package cn.zhxu.toys.msg;

/**
 * @author Troy.Zhou @ 2022/8/17 21:25
 */
public class RateLimitException extends RuntimeException {

    public RateLimitException() {
        super();
    }

    public RateLimitException(String message) {
        super(message);
    }

    public RateLimitException(String message, Throwable cause) {
        super(message, cause);
    }

}

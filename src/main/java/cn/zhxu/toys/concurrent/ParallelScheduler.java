package cn.zhxu.toys.concurrent;

import java.util.List;
import java.util.concurrent.Future;

/**
 * 平行机调度
 * @author Troy.Zhou
 * @since v0.3.3
 */
public interface ParallelScheduler {

    /**
     * 任务提供器
     * @param <T> 任务类型
     */
    interface TaskProvider<T> {

        List<T> getTaskList(int page, int size);

    }
    
    /**
     * 任务执行器
     * @param <T> 任务类型
     */
    interface TaskExecutor<T> {

        void execute(T task);

    }
    
    /**
     * 任务标识器
     *
     * @param <T> 任务类型
     */
    interface Identify<T> {

        long id(T t);

    }
    
    class SelfIdentify<T extends Number> implements Identify<T> {

		@Override
		public long id(T t) {
			return t.longValue();
		}
    	
    }
    
	/**
     * 同步调度平行机
     * @param concurrency 最大并发量
     * @param provider 任务提供者
     * @param executor 任务执行器
     */
    <T> void schedule(int concurrency, TaskProvider<T> provider, TaskExecutor<T> executor);

	/**
     * 同步调度平行机
     * @param concurrency 最大并发量
     * @param provider 任务提供者
     * @param executor 任务执行器
     * @param identify 任务标识器，用于对 provider 提供的任务进行去重
     */
    <T> void schedule(int concurrency, TaskProvider<T> provider, TaskExecutor<T> executor, Identify<T> identify);
    
	/**
     * 异步调度平行机
     * @param concurrency 最大并发量
     * @param provider 任务提供者
     * @param executor 任务执行器
     */
    <T> Future<?> asyncSchedule(int concurrency, TaskProvider<T> provider, TaskExecutor<T> executor);

	/**
     * 异步调度平行机
     * @param concurrency 最大并发量
     * @param provider 任务提供者
     * @param executor 任务执行器
     * @param identify 任务标识器，用于对provider提供的任务进行去重
     */
    <T> Future<?> asyncSchedule(int concurrency, TaskProvider<T> provider, TaskExecutor<T> executor, Identify<T> identify);

}

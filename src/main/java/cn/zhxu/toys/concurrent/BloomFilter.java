package cn.zhxu.toys.concurrent;

/**
 * 布隆过滤器
 * @author Administrator
 *
 */
public interface BloomFilter {

	
	/**
	 * @param key 业务键
	 * @param object 对象
	 */
	void put(String key, Object object);
	
	

	/**
	 * @param key 业务键
	 * @param object 对象
	 * @return {@code true} if the element <i>might</i> have been put in this Bloom filter, {@code false} if this is <i>definitely</i> not the case.
	 */
	boolean mightContain(String key, Object object);

	/**
	 * 哈希器
	 * @author Administrator
	 *
	 */
	interface Hasher {
		
		
		/**
		 * @param object 对象
		 * @param funIndex 函数下标
		 * @return object 的 第 funIndex Hash 值
		 */
		long hash(Object object, int funIndex);
		
		
	}
	
	
	
}

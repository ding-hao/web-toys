package cn.zhxu.toys.concurrent;

import java.util.concurrent.Callable;

/**
 * 分布式并发同步锁
 * 
 * @author Troy
 * 
 * @since 0.1.6
 */
public interface SyncLock {

	/**
	 * 获取锁
	 * @param name 锁名称
	 */
	void lock(String name);
	
	/**
	 * 获取锁
	 * @since 0.3.2
	 * @param name 锁名称
	 * @param maxHoldSeconds 锁最大持有时间
	 */
	void lock(String name, int maxHoldSeconds);
	
	/**
	 * 获取锁
	 * @since 0.3.2
	 * @param name 锁名称
	 * @param maxHoldSeconds 持有锁最大时间
	 * @param maxWaitSeconds 等待锁最大时间
	 */
	void lock(String name, int maxHoldSeconds, int maxWaitSeconds);
	
	/**
	 * 获取锁
	 * @param name 锁名称
	 * @param requestId 请求者
	 */
	void lock(String name, String requestId);
	
	/**
	 * 获取锁
	 * @param name 锁名称
	 * @param requestId 请求者
	 * @param maxHoldSeconds 锁最大持有时间
	 */
	void lock(String name, String requestId, int maxHoldSeconds);
	
	/**
	 * 获取锁
	 * @param name 锁名称
	 * @param requestId 请求者
	 * @param maxHoldSeconds 持有锁最大时间
	 * @param maxWaitSeconds 等待锁最大时间
	 */
	void lock(String name, String requestId, int maxHoldSeconds, int maxWaitSeconds);
	
	/**
	 * 释放锁
	 * @param name 锁名称
	 */
	void release(String name);
	
	/**
	 * 释放锁
	 * @param name 锁名称
	 * @param requestId 请求者
	 */
	void release(String name, String requestId);
	
	/**
	 * 在锁内运行
	 * @since v0.4.9
	 * @param name 锁名称
	 * @param run 任务
	 */
	void with(String name, Runnable run);
	
	/**
	 * 在锁内运行
	 * @since v0.4.9
	 * @param name 锁名称
	 * @param maxHoldSeconds 锁最大持有时间
	 * @param run 任务
	 */
	void with(String name, int maxHoldSeconds, Runnable run);
	
	/**
	 * 在锁内运行
	 * @since v0.4.9
	 * @param name 锁名称
	 * @param maxHoldSeconds 持有锁最大时间
	 * @param maxWaitSeconds 等待锁最大时间
	 * @param run 任务
	 */
	void with(String name, int maxHoldSeconds, int maxWaitSeconds, Runnable run);
	
	/**
	 * 在锁内运行
	 * @since v0.4.9
	 * @param name 锁名称
	 * @param requestId 请求者
	 * @param run 任务
	 */
	void with(String name, String requestId, Runnable run);
	
	/**
	 * 在锁内运行
	 * @since v0.4.9
	 * @param name 锁名称
	 * @param requestId 请求者
	 * @param maxHoldSeconds 锁最大持有时间
	 * @param run 任务
	 */
	void with(String name, String requestId, int maxHoldSeconds, Runnable run);
	
	/**
	 * 在锁内运行
	 * @since v0.4.9
	 * @param name 锁名称
	 * @param requestId 请求者
	 * @param maxHoldSeconds 持有锁最大时间
	 * @param maxWaitSeconds 等待锁最大时间
	 * @param run 任务
	 */
	void with(String name, String requestId, int maxHoldSeconds, int maxWaitSeconds, Runnable run);

	
	/**
	 * 在锁内运行
	 * @since v0.4.9
	 * @param name 锁名称
	 * @param run 任务
	 * @return the result of run
	 */
	<V> V run(String name, Callable<V> run);
	
	/**
	 * 在锁内运行
	 * @since v0.4.9
	 * @param name 锁名称
	 * @param maxHoldSeconds 锁最大持有时间
	 * @param run 任务
	 * @return the result of run
	 */
	<V> V run(String name, int maxHoldSeconds, Callable<V> run);
	
	/**
	 * 在锁内运行
	 * @since v0.4.9
	 * @since 0.3.2
	 * @param name 锁名称
	 * @param maxHoldSeconds 持有锁最大时间
	 * @param maxWaitSeconds 等待锁最大时间
	 * @param run 任务
	 * @return the result of run
	 */
	<V> V run(String name, int maxHoldSeconds, int maxWaitSeconds, Callable<V> run);
	
	/**
	 * 在锁内运行
	 * @since v0.4.9
	 * @param name 锁名称
	 * @param requestId 请求者
	 * @param run 任务
	 * @return the result of run
	 */
	<V> V run(String name, String requestId, Callable<V> run);
	
	/**
	 * 在锁内运行
	 * @since v0.4.9
	 * @param name 锁名称
	 * @param requestId 请求者
	 * @param maxHoldSeconds 锁最大持有时间
	 * @param run 任务
	 * @return the result of run
	 */
	<V> V run(String name, String requestId, int maxHoldSeconds, Callable<V> run);
	
	/**
	 * 在锁内运行
	 * @since v0.4.9
	 * @param name 锁名称
	 * @param requestId 请求者
	 * @param maxHoldSeconds 持有锁最大时间
	 * @param maxWaitSeconds 等待锁最大时间
	 * @param run 任务
	 * @return the result of run
	 */
	<V> V run(String name, String requestId, int maxHoldSeconds, int maxWaitSeconds, Callable<V> run);

}

package cn.zhxu.toys.cipher;

import cn.zhxu.toys.util.DigestUtils;


/**
 * 简单加密算法 2
 * @author Troy.Zhou
 * @since 0.4.6
 */
public class SIM2Encryptor extends SimpleEncryptor {

	public SIM2Encryptor() {}
	
	public SIM2Encryptor(byte[] secret) {
		doInit(secret);
	}

	public SIM2Encryptor(String secret) {
		init(secret);
	}
	
	@Override
	protected void doInit(byte[] secret) {
		this.secret = DigestUtils.toMd5(secret);
	}

	@Override
	public byte[] encrypt(byte[] secretBytes, byte[] plainBytes) {	
		secretBytes = DigestUtils.toMd5(secretBytes);
		return doEncrypt(secretBytes, plainBytes);
	}

}

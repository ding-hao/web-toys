package cn.zhxu.toys.cipher;


/**
 * 解密器
 * 
 * @author Administrator
 *
 */
public interface Decryptor {

	
	/**
	 * 初始化
	 * @param secret
	 */
	void init(String secret);
	
	
	/**
	 * 解密
	 * @param cipherBytes 密文
	 * @return 明文
	 */
	byte[] decrypt(byte[] cipherBytes);
	
	/**
	 * 解密
	 * @param secretBytes 密钥
	 * @param cipherBytes 密文
	 * @return 明文
	 */
	byte[] decrypt(byte[] secretBytes, byte[] cipherBytes);
	
	
	/**
	 * 解密
	 * @param cipherText 密文
	 * @return 明文
	 */
	String decrypt(String cipherText);
	
	/**
	 * 解密
	 * @param secretText 密钥
	 * @param cipherText 密文
	 * @return 明文
	 */
	String decrypt(String secretText, String cipherText);
	
	
}

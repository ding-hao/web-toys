package cn.zhxu.toys.cipher;

import cn.zhxu.toys.util.DigestUtils;


/**
 * 简单解密算法 2
 * @author Troy.Zhou
 * @since 0.4.6
 */
public class SIM2Decryptor extends SimpleDecryptor {

	public SIM2Decryptor() {}
	
	public SIM2Decryptor(byte[] secret) {
		doInit(secret);
	}
	
	public SIM2Decryptor(String secret) {
		init(secret);
	}

	@Override
	protected void doInit(byte[] secret) {
		this.secret = DigestUtils.toMd5(secret);
	}

	@Override
	public byte[] decrypt(byte[] secretBytes, byte[] cipherBytes) {
		secretBytes = DigestUtils.toMd5(secretBytes);
		return doDecrypt(secretBytes, cipherBytes);
	}
	
}

package cn.zhxu.toys.cipher;



/**
 * 加密器
 * @author Administrator
 *
 */
public interface Encryptor {

	
	/**
	 * 初始化
	 * @param secret
	 */
	void init(String secret);
	
	
	/**
	 * 加密
	 * @param plainBytes 明文
	 * @return
	 */
	byte[] encrypt(byte[] plainBytes);
	
	/**
	 * 加密
	 * @param secretBytes 密钥
	 * @param plainBytes 明文
	 * @return
	 */
	byte[] encrypt(byte[] secretBytes, byte[] plainBytes);
	
	/**
	 * 加密
	 * @param plainText
	 * @return
	 */
	String encrypt(String plainText);
	
	
	/**
	 * 加密
	 * @param secretText 密钥
	 * @param plainText 明文
	 * @return
	 */
	String encrypt(String secretText, String plainText);
	

	
	
}

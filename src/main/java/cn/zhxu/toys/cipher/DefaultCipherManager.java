package cn.zhxu.toys.cipher;

import org.springframework.beans.factory.InitializingBean;


public class DefaultCipherManager implements CipherManager, InitializingBean {


	private String secret;
	
	
	private Encryptor encryptor;
	
	private Decryptor decryptor;

	
	@Override
	public void afterPropertiesSet() throws Exception {
		init(secret);
	}

	
	@Override
	public void init(String secret) {
		encryptor.init(secret);
		decryptor.init(secret);
	}
	
	@Override
	public String encrypt(String plainText) {
		return encryptor.encrypt(plainText);
	}

	@Override
	public String encrypt(String secret, String plainText) {
		return encryptor.encrypt(secret, plainText);
	}

	@Override
	public byte[] encrypt(byte[] secretBytes, byte[] plainBytes) {
		return encryptor.encrypt(secretBytes, plainBytes);
	}
	
	@Override
	public String decrypt(String cipherText) {
		return decryptor.decrypt(cipherText);
	}

	@Override
	public String decrypt(String secret, String cipherText) {
		return decryptor.decrypt(secret, cipherText);
	}
	
	@Override
	public byte[] encrypt(byte[] plainBytes) {
		return encryptor.encrypt(plainBytes);
	}

	@Override
	public byte[] decrypt(byte[] cipherBytes) {
		return decryptor.decrypt(cipherBytes);
	}

	@Override
	public byte[] decrypt(byte[] secretBytes, byte[] cipherBytes) {
		return decryptor.decrypt(cipherBytes, cipherBytes);
	}


	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public Encryptor getEncryptor() {
		return encryptor;
	}

	public void setEncryptor(Encryptor encryptor) {
		this.encryptor = encryptor;
	}

	public Decryptor getDecryptor() {
		return decryptor;
	}

	public void setDecryptor(Decryptor decryptor) {
		this.decryptor = decryptor;
	}

}

package cn.zhxu.toys.util;

import java.util.Arrays;

/**
 * 
 * @author Troy
 *
 * @since 0.4.6
 */
public class ByteUtils {

    public static int unsigned(byte value) {
        if (value >= 0) {
            return value;
        }
        return 256 + value;
    }

    public static boolean equals(byte[] arr1, byte[] arr2) {
        if (arr1 == null && arr2 == null) {
            return true;
        }
        if (arr1 == null || arr2 == null || arr1.length != arr2.length) {
            return false;
        }
        for (int i= 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                return false;
            }
        }
        return true;
    }


    public static byte[][] split(byte[] arr, byte separator) {
        int count = countOf(arr, separator);
        byte[][] result = new byte[count + 1][];
        int index = indexOf(arr, separator, 0, arr.length);
        if (index < 0) {
            result[0] = arr;
        } else {
            result[0] = subArray(arr, 0, index);
            for (int i = 1; i <= count; i++) {
                int from = index + 1;
                index = indexOf(arr, separator, from, arr.length);
                if (index < from) {
                    index = arr.length;
                }
                result[i] = subArray(arr, from, index);
            }
        }
        return result;
    }

    /**
     * 寻找目标字节在字节数组中的下标
     * @param arr 数组
     * @param target 目标字节
     * @param from 检索开始下标（包含）
     * @param to 检索结束下标（不包含）
     * @return 找不到则返回-1
     */
    public static int indexOf(byte[] arr, byte target, int from, int to) {
        for (int i = from; i < to; i++) {
            if (arr[i] == target) {
                return i;
            }
        }
        return -1;
    }

    public static int countOf(byte[] arr, byte target) {
        int count = 0;
        for (byte b : arr) {
            if (b == target) {
                count++;
            }
        }
        return count;
    }

    public static byte[] subArray(byte[] arr, int from) {
        return subArray(arr, from, arr.length);
    }

    public static byte[] subArray(byte[] arr, int from, int to) {
        byte[] result = new byte[to - from];
        if (to > from) {
            System.arraycopy(arr, from, result, 0, result.length);
        }
        return result;
    }

    public static byte[] join(byte hyphen, byte[]... arrs) {
        int length = 0;
        for (byte[] arr: arrs) {
            length += arr.length;
        }
        byte[] result = new byte[length + arrs.length - 1];
        int index = 0;
        for (int i = 0; i < arrs.length; i++) {
            byte[] arr = arrs[i];
            System.arraycopy(arr, 0, result, index, arr.length);
            index += arr.length;
            if (i < arrs.length - 1) {
                result[index] = hyphen;
                index++;
            }
        }
        return result;
    }

    public static byte[] join(byte[]... arrs) {
        int length = 0;
        for (byte[] arr : arrs) {
            length += arr.length;
        }
        byte[] result = new byte[length];
        int index = 0;
        for (byte[] arr : arrs) {
            System.arraycopy(arr, 0, result, index, arr.length);
            index += arr.length;
        }
        return result;
    }

    public static byte[] trim(byte[] src, byte empty) {
        return trim(src, empty, 0, src.length);
    }

    public static byte[] trim(byte[] src, byte empty, int from, int to) {
        while (from < src.length - 1 && src[from] == empty) {
            from++;
        }
        while (to > from && src[to - 1] == empty) {
            to--;
        }
        return subArray(src, from, to);
    }

    public static String bcd(byte[] src, int from, int to) {
        char[] chars = new char[2 * (to - from)];
        for (int i = from; i < to; i++) {
            int b = unsigned(src[i]);
            chars[2 * (i - from)] = (char) ((b >> 4) + 0x30);
            chars[2 * (i - from) + 1] = (char) ((b & 0xF) + 0x30);
        }
        return new String(chars);
    }
    
    /**
     * 异或值，返回
     * @param bytes 数组
     * @return 异或值
     */
    public static int xor(byte[] bytes) {
    	// 公式： 0 ^ A = A
        int temp = 0;
        if (bytes != null) {
            for (byte aByte : bytes) {
                temp ^= aByte;
            }
        }
        return temp;
    }

    public static byte[] hexToBytes(String hexContent) {
        if (StringUtils.isBlank(hexContent) || hexContent.length() % 2 != 0) {
            throw new IllegalArgumentException();
        }
        // 2个16进制字符=>1个字节
        byte[] bytes = new byte[hexContent.length() / 2];
        char[] chars = hexContent.toCharArray();
        for (int i = 0; i < hexContent.length() / 2; i++) {
            int index = i * 2;
            byte a = (byte) Character.digit(chars[index],16); // 高4
            byte b = (byte) Character.digit(chars[index + 1], 16); //低四
            bytes[i] = (byte) (a << 4 | b);
        }
        return bytes;
    }

    public static void main(String[] args) {
        byte[] bytes = {100,5,12,13,15,30,120};
        String hexStr = StringUtils.toHexStr(bytes);
        System.out.println("转成hex字符串" + hexStr);
        System.out.println( "转成字节数组 " +Arrays.toString(hexToBytes(hexStr)));
    }

}

package cn.zhxu.toys.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class StringUtils {

	public static char[] hexChars = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

	public static String toHexStr(byte[] bytes) {
        char[] str = new char[2 * bytes.length];
        int k = 0;
		for (byte b : bytes) {
			str[k++] = hexChars[b >>> 4 & 0xf];    //高4位
			str[k++] = hexChars[b & 0xf];        //低4位
		}
        return new String(str);
	}
	
	/**
	 * 首字母变小写
	 */
	public static String firstCharToLowerCase(String str) {
		char firstChar = str.charAt(0);
		if (firstChar >= 'A' && firstChar <= 'Z') {
			char[] arr = str.toCharArray();
			arr[0] += ('a' - 'A');
			return new String(arr);
		}
		return str;
	}
	
	/**
	 * 首字母变大写
	 */
	public static String firstCharToUpperCase(String str) {
		char firstChar = str.charAt(0);
		if (firstChar >= 'a' && firstChar <= 'z') {
			char[] arr = str.toCharArray();
			arr[0] -= ('a' - 'A');
			return new String(arr);
		}
		return str;
	}
	
	/**
	 * 字符串为 null 或者内部字符全部为 ' ' '\t' '\n' '\r' 这四类字符时返回 true
	 */
	public static boolean isBlank(String str) {
		if (str == null) {
			return true;
		}
		int len = str.length();
		if (len == 0) {
			return true;
		}
		for (int i = 0; i < len; i++) {
			switch (str.charAt(i)) {
			case ' ':
			case '\t':
			case '\n':
			case '\r':
			// case '\b':
			// case '\f':
				break;
			default:
				return false;
			}
		}
		return true;
	}
	
	public static boolean notBlank(String str) {
		return !isBlank(str);
	}
	
	public static boolean notBlank(String... strings) {
		if (strings == null) {
			return false;
		}
		for (String str : strings) {
			if (isBlank(str)) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean notNull(Object... paras) {
		if (paras == null) {
			return false;
		}
		for (Object obj : paras) {
			if (obj == null) {
				return false;
			}
		}
		return true;
	}
	
	public static String join(String[] stringArray) {
		StringBuilder sb = new StringBuilder();
		for (String s : stringArray) {
			sb.append(s);
		}
		return sb.toString();
	}

	
	public static String getRandomUUID() {
		return java.util.UUID.randomUUID().toString().replace("-", "");
	}
	
	
    public static String join(String[] strAry, String separator) {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < strAry.length; i++){
            if(i == (strAry.length-1)) {
                sb.append(strAry[i]);
            } else{
                sb.append(strAry[i]).append(separator);
            }
        }
        return new String(sb);
    }
    
    
    public static String join(long[] array, String separator) {
    	Object[] arr = new Object[array.length];
    	for (int i = 0; i < array.length; i++) {
    		arr[i] = array[i];
    	}
    	return join(arr, separator);
    }
    
    
    public static String join(Object[] array, String separator) {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < array.length; i++){
            if(i == (array.length-1)) {
                sb.append(array[i]);
            } else{
                sb.append(array[i]).append(separator);
            }
        }
        return new String(sb);
    }
	
    public static String getLastByToken(String src, String token) {
    	if (src == null) {
    		throw new IllegalArgumentException("src 不能为 null");
    	}
    	String[] splits = src.split(token);
    	return splits[splits.length - 1];
    }
    

	public static String mask(String src, int from, char mask) {
		return mask(src, from, mask, src.length());
	}
	
	
	public static String mask(String src, char mask, int to) {
		return mask(src, 0, mask, to);
	}
	
	
	public static String mask(String src, int from, char mask, int to) {
		if (src == null) {
			return null;
		}
		if (to < from) {
			throw new IllegalArgumentException("'from' must less than 'to'!");
		}
		if (from < 0) {
			throw new IllegalArgumentException("'from' must greater than or equal 'to'!");
		}
		if (to > src.length()) {
			throw new IllegalArgumentException("'to' must less than or equal the length of 'src'!");
		}
		StringBuilder sb = new StringBuilder(src.substring(0, from));
		for (int i = from; i < to; i++) {
			sb.append(mask);
		}
		sb.append(src.substring(to));
		return sb.toString();
	}
	
	public static String formatDateTimeWithMiliSecond(Date date) {
		DateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		return sdf.format(date);
	}
	
	
	public static String formatCode(long id) {
		return strFrom(id, 4) + formatDateTimeWithMiliSecond(new Date()) + randomNumString(5);
	}

	public static String strFrom(long code, int width) {
		StringBuilder str = new StringBuilder("" + code);
		while (str.length() < width) {
			str.insert(0, "0");
		}
		if (str.length() > width) {
			str = new StringBuilder(str.substring(str.length() - width));
		}
		return str.toString();
	}
	
	public static String randomNumString(int width) {
		Random random = new Random();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < width; i++) {
			sb.append(random.nextInt(10));
		}
		return sb.toString();
	}
	
	public static String toRmbStr(long amount) {
	  	DecimalFormat df = new DecimalFormat("0.00");
	  	return df.format(amount / 100D);
	}
	
	
	/**
	 * 连字符风格转驼峰风格
	 * @param src 源字符串
	 * @param hyphenation 源字符串中的连字符
	 * @param initLetterLower 是否首字母小写
	 * @return String
	 */
	public static String toCamera(String src, String hyphenation, boolean initLetterLower) {
		if (isBlank(src)) {
			return src;
		}
		StringBuilder sb = new StringBuilder();
		String[] list = src.split(hyphenation);
		for (int i = 0; i < list.length; i++) {
			if (i == 0 && initLetterLower) {
				sb.append(firstCharToLowerCase(list[i]));
			} else {
				sb.append(firstCharToUpperCase(list[i]));
			}
		}
		return sb.toString();
	}
	
	/**
	 * 连字符风格转驼峰风格
	 * @param src 源字符串
	 * @param hyphenation 源字符串中的连字符
	 * @return String
	 */
	public static String toCamera(String src, String hyphenation) {
		return toCamera(src, hyphenation, true);
	}
	
	/**
	 * 下划线连字符风格转驼峰风格
	 * @param src 源字符串
	 * @return String
	 */
	public static String toCamera(String src) {
		return toCamera(src, "_", true);
	}
	
	/**
	 * 驼峰风格风格转连字符风格
	 * @param src src
	 * @param hyphenation hyphenation
	 * @return String
	 */
	public static String toHyphenation(String src, String hyphenation) {
        StringBuilder sb = new StringBuilder(src);
        int temp = 0;//定位
        for(int i = 0; i < src.length(); i++){
            if(Character.isUpperCase(src.charAt(i))){
                sb.insert(i + temp, hyphenation);
                temp += 1;
            }
        }
        return sb.toString().toLowerCase();
	}
	
	/**
	 * 驼峰风格风格转下划线风格
	 * @param src src
	 * @return String
	 */
	public static String toHyphenation(String src) {
		return toHyphenation(src, "_");
	}


	/**
	 * 判断一个字符串是否包含 start 与 end 之间的字符
	 * @param src 字符串
	 * @param start 目标起始边界字符（包含）
	 * @param end 目标结束边界字符（包含）
	 * @return true if src contain a character that between start and end
	 */
	public static boolean hasCharacter(String src, char start, char end) {
		for (int i = 0; i < src.length(); i ++) {
			char c = src.charAt(i);
			if (c >= start && c <= end) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断一个字符串是否包含大写字母
	 * @param src 字符串
	 * @return true if src contain a upper case character
	 */
	public static boolean hasUpperCase(String src) {
		return hasCharacter(src, 'A', 'Z');
	}

	/**
	 * 判断一个字符串是否包含小写字母
	 * @param src 字符串
	 * @return true if src contain a lower case character
	 */
	public static boolean hasLowerCase(String src) {
		return hasCharacter(src, 'a', 'z');
	}

	/**
	 * 判断一个字符串是否包含数字（0 ~ 9）字符
	 * @param src 字符串
	 * @return true if src contain a num character
	 */
	public static boolean hasNumChar(String src) {
		return hasCharacter(src, '0', '9');
	}

}

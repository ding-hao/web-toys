package cn.zhxu.toys.util;

import java.io.Serializable;
import java.util.Map.Entry;


public class KeyValue<K, V> implements Entry<K, V>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1775205527301045834L;
	
	
	private K key;
	private V value;
	
	public KeyValue(K key, V value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public K getKey() {
		return key;
	}

	@Override
	public V getValue() {
		return value;
	}

	@Override
	public V setValue(V value) {
		return this.value = value;
	}
	
}
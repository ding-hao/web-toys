package cn.zhxu.toys.captcha;


/**
 * 验证码存储接口
 * 
 * @author Troy
 *
 */
public interface CaptchaStorage {

	/**
	 * 根据手机号取得验证码
	 */
	Captcha findCaptcha(String captchaId);

	/**
	 * 保存验证码
	 */
	void save(String captchaId, String code, int expireSeconds);

	/**
	 * 删除验证码
	 */
	void delete(String captchaId);
	
}

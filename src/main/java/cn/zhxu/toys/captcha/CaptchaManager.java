package cn.zhxu.toys.captcha;

/**
 * 验证码管理器
 * @author Troy.Zhou
 */
public interface CaptchaManager {

	enum VerifyResult {
		SUCCESS, 	// 成功
		INVALID, 	// 无效验证码
		EXPIRED  	// 已过期
	}

	/**
	 * 生成并渲染验证码
	 * @param captchaId 验证码标识（发短信验证时，是手机号，图形验证码时，是请求ID）
	 * @param attrs 附加属性
	 * @throws CaptchaException 异常
	 */
	void genAndRender(String captchaId, CaptchaAttrs attrs) throws CaptchaException;

	/**
	 * 校验验证码
	 * @param captchaId 验证码标识（发短信验证时，是手机号，图形验证码时，是请求ID）
	 * @param code 验证码
	 * @return 校验结果
	 */
	VerifyResult verify(String captchaId, String code);

	/**
	 * 校验验证码，校验失败时抛出异常
	 * @param captchaId 验证码标识（发短信验证时，是手机号，图形验证码时，是请求ID）
	 * @param code 验证码
	 * @throws CaptchaException 建议失败异常
	 * @since v0.0.2
	 */
	default void assertCode(String captchaId, String code) throws CaptchaException {
		VerifyResult result = verify(captchaId, code);
		if (result == VerifyResult.INVALID) {
			throw new CaptchaException("验证码不正确！");
		}
		if (result == VerifyResult.EXPIRED) {
			throw new CaptchaException("验证码已过期！");
		}
	}

}

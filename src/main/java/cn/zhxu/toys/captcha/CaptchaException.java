package cn.zhxu.toys.captcha;

/**
 * @author Troy.Zhou @ 2022/8/16 15:02
 */
public class CaptchaException extends RuntimeException {

    public CaptchaException(String message) {
        super(message);
    }

    public CaptchaException(String message, Throwable cause) {
        super(message, cause);
    }

}

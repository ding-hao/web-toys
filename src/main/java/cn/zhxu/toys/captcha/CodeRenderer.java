package cn.zhxu.toys.captcha;

/**
 * 验证码渲染器
 * @author Troy.Zhou @ 2022/8/16 13:58
 */
public interface CodeRenderer {

    /**
     * 输出指定验证码图片流
     * @param code 验证码值
     * @param attrs 附加属性
     * @throws CaptchaException 异常
     */
    void render(String code, CaptchaAttrs attrs) throws CaptchaException;

}

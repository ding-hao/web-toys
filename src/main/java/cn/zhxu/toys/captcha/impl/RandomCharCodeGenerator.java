package cn.zhxu.toys.captcha.impl;

import cn.zhxu.toys.captcha.CodeGenerator;

import java.util.Random;

/**
 * 随机字符生成器
 * Created by junxiang on 2019/8/2 0002
 */
public class RandomCharCodeGenerator implements CodeGenerator {

    /**
     * 只显示大写，去掉了1,0,i,o几个容易混淆的字符
     */
    private static final String CHARS = "23456789ABCDEFGHJKLMNPQRSTUVWXYZ";

    private final Random random = new Random();

    private int length = 4;

    public RandomCharCodeGenerator() {
    }

    public RandomCharCodeGenerator(int length) {
        this.length = length;
    }

    @Override
    public CodeResult generate() {
        StringBuilder sb = new StringBuilder(length);
        for(int i = 0; i < length; i++){
            sb.append(CHARS.charAt(random.nextInt(CHARS.length() - 1)));
        }
        String code = sb.toString();
        return new CodeResult(code, code);
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

}

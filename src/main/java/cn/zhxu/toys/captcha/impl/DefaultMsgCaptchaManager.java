package cn.zhxu.toys.captcha.impl;

import cn.zhxu.toys.captcha.CaptchaAttrs;
import cn.zhxu.toys.captcha.CaptchaException;
import cn.zhxu.toys.captcha.MsgCaptchaManager;
import org.springframework.beans.factory.InitializingBean;

/**
 * 默认验证码管理器
 * 
 * @author Troy.Zhou
 *
 */
public class DefaultMsgCaptchaManager extends BaseCaptchaManager implements MsgCaptchaManager, InitializingBean {

	@Override
	public void genAndRender(String phone) throws CaptchaException {
		CaptchaAttrs attrs = CaptchaAttrs.newAttrs()
				.with(MsgCodeRenderer.ATTR_PHONE, phone);
		genAndRender(phone, attrs);
	}

	@Override
	public void genAndRender(String phone, String msgTmplName) throws CaptchaException {
		CaptchaAttrs attrs = CaptchaAttrs.newAttrs()
				.with(MsgCodeRenderer.ATTR_PHONE, phone)
				.with(MsgCodeRenderer.ATTR_MSG_TMPL_NAME, msgTmplName);
		genAndRender(phone, attrs);
	}

}

package cn.zhxu.toys.captcha.impl;

import cn.zhxu.toys.captcha.CodeGenerator;

import java.util.Random;

/**
 * 
 * 随机数字验证码生成器
 * 
 * @author Troy
 *
 */
public class RandomNumCodeGenerator implements CodeGenerator {

	
	private final Random random = new Random();

	private int length = 6;

	public RandomNumCodeGenerator() {
	}

	public RandomNumCodeGenerator(int length) {
		this.length = length;
	}

	@Override
	public CodeResult generate() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < length; i++) {
			sb.append(random.nextInt(10));
		}
		String code = sb.toString();
		return new CodeResult(code, code);
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
	
}

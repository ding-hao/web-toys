package cn.zhxu.toys.captcha.impl;

import cn.zhxu.toys.captcha.CaptchaAttrs;
import cn.zhxu.toys.captcha.CaptchaException;
import cn.zhxu.toys.captcha.ImageCaptchaManager;

import java.io.OutputStream;

/**
 * 图形验证码
 * Created by junxiang on 2019/8/2 0002
 */
public class DefaultImageCaptchaManager extends BaseCaptchaManager implements ImageCaptchaManager {

	public DefaultImageCaptchaManager() {
		setCodeRenderer(new ImageCodeRenderer());
	}

	@Override
    public void genAndRender(String captchaId, OutputStream os) throws CaptchaException {
		CaptchaAttrs attrs = CaptchaAttrs.newAttrs()
				.with(ImageCodeRenderer.ATTR_OUTPUT_STREAM, os);
		genAndRender(captchaId, attrs);
    }

	@Override
	public void genAndRender(String captchaId, OutputStream os, int width, int height) throws CaptchaException {
		CaptchaAttrs attrs = CaptchaAttrs.newAttrs()
				.with(ImageCodeRenderer.ATTR_OUTPUT_STREAM, os)
				.with(ImageCodeRenderer.ATTR_WIDTH, width)
				.with(ImageCodeRenderer.ATTR_HEIGHT, height);
		genAndRender(captchaId, attrs);
	}

}

package cn.zhxu.toys.captcha.impl;

import cn.zhxu.toys.captcha.Captcha;

/**
 * 验证码实体
 * 
 * @author Troy
 *
 */
public class CaptchaBean implements Captcha {

	private String code;
	private long expireTime;

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public boolean isExpired() {
		return expireTime < System.currentTimeMillis() / 1000;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setExpireTime(long expireTime) {
		this.expireTime = expireTime;
	}

	public String serialize() {
		return expireTime + "," + code;
	}

	public static CaptchaBean parse(String str) {
		int idx = str.indexOf(",");
		if (idx > 0 && idx < str.length() - 1) {
			CaptchaBean bean = new CaptchaBean();
			bean.setExpireTime(Long.parseLong(str.substring(0, idx)));
			bean.setCode(str.substring(idx + 1));
			return bean;
		}
		return null;
	}

}

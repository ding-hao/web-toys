package cn.zhxu.toys.captcha.impl;

import cn.zhxu.toys.captcha.CodeGenerator;

import java.util.Random;

/**
 * 运算式生成器
 * Created by junxiang on 2019/8/2 0002
 */
public class RandomFormulaCodeGenerator implements CodeGenerator {

    /**
     * 数字0-9
     */
    private static final String NUM_CHAR = "0123456789";

    /**
     *  加减乘除四种运算
     */
    private static final String OPERATORS = "+,-,*,÷";

    private static final String ADDITION = "+";

    private static final String SUBTRACTION = "-";

    private static final String MULTIPLY = "*";

    private static final String DIVISION = "÷";

    private static final String EQUAL_SIGN = "=";

    private final Random random = new Random();


    @Override
    public CodeResult generate() {
        String operator = getOperator();
        int[] nums = getNumber(operator);
        int resultNum = getResult(nums[0], nums[1], operator);
        String formula = nums[0] + operator + nums[1] + EQUAL_SIGN;
        String check = String.valueOf(resultNum);
        return new CodeResult(formula, check);
    }

    /**
     * 获取随机数
     * @param operator 运算符
     * @return 随机数
     */
    private int[] getNumber(String operator) {
        int[] numArray = new int[2];
        int firstNum = NUM_CHAR.charAt(random.nextInt(NUM_CHAR.length() - 1)) - '0';
        int secondNum = NUM_CHAR.charAt(random.nextInt(NUM_CHAR.length() - 1)) - '0';
        if (SUBTRACTION.equals(operator) || DIVISION.equals(operator)) {
            numArray[0] = Math.max(firstNum, secondNum);
            numArray[1] = Math.min(firstNum, secondNum);
            if (DIVISION.equals(operator)) {
                while (numArray[1] == 0 || numArray[0] % numArray[1] > 0) {
                    firstNum = NUM_CHAR.charAt(random.nextInt(NUM_CHAR.length() - 1)) - '0';
                    secondNum = NUM_CHAR.charAt(random.nextInt(NUM_CHAR.length() - 1)) - '0';
                    numArray[0] = Math.max(firstNum, secondNum);
                    numArray[1] = Math.min(firstNum, secondNum);
                }
            }
        } else {
            numArray[0] = firstNum;
            numArray[1] = secondNum;
        }
        return numArray;
    }

    /**
     * @return 运算符
     */
    private String getOperator() {
        String[] vcArray = OPERATORS.split(",");
        int iNum = random.nextInt(vcArray.length);
        return vcArray[iNum];
    }

    /**
     *  获取运算结果
     * @param firstNum 运算式第一个值
     * @param secondNum 运算式第二个值
     * @param operator 运算符
     * @return 运算式结果
     */
    private int getResult(int firstNum, int secondNum, String operator) {
        int result = 0;
        switch (operator) {
            case ADDITION:
                result = firstNum + secondNum;
                break;
            case SUBTRACTION:
                result = firstNum - secondNum;
                break;
            case MULTIPLY:
                result = firstNum * secondNum;
                break;
            case DIVISION:
                result = firstNum / secondNum;
                break;
        }
        return result;
    }

}

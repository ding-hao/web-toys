package cn.zhxu.toys.captcha;

/**
 * 
 * 短信验证码管理器
 * 
 * @author Troy.Zhou
 *
 */
public interface MsgCaptchaManager extends CaptchaManager {
	
	/**
	 * 发送验证码
	 * @param phone 手机号 
	 * @throws CaptchaException 异常
	 */
	void genAndRender(String phone) throws CaptchaException;
	
	/**
	 * 发送验证码
	 * @param phone 手机号 
	 * @param msgTmplName 使用的模板名
	 */
	void genAndRender(String phone, String msgTmplName) throws CaptchaException;
	
}

package cn.zhxu.toys.captcha;


/**
 * 验证码
 * 
 * @author Troy
 *
 */
public interface Captcha {

	/**
	 * @return 验证码
	 */
	String getCode();
	
	/**
	 * @return 是否过期
	 */
	boolean isExpired();

}

package cn.zhxu.toys.captcha;


/**
 * 
 * 验证码生成器
 * 
 * @author Troy
 *
 */
public interface CodeGenerator {

	class CodeResult {

		private final String code;
		private final String check;

		/**
		 * @param code 需要渲染到前端的验证码
		 * @param check 用于检查对错的检查码
		 */
		public CodeResult(String code, String check) {
			this.code = code;
			this.check = check;
		}

		/**
		 * 需要渲染到前端的验证码
		 */
		public String getCode() {
			return code;
		}

		/**
		 * 用于检查对错的检查码
		 */
		public String getCheck() {
			return check;
		}

	}

	/***
	 * 生成一个验证码
	 * @return CodeResult
	 */
	CodeResult generate();
	
}

package cn.zhxu.toys.captcha;

import java.io.OutputStream;

/**
 * Created by junxiang on 2019/8/2 0002
 */
public interface ImageCaptchaManager extends CaptchaManager {

    /**
     * 输出随机验证码图片流,并返回验证码值
     * @param captchaId 验证码标识
     * @param os 输出流
     * @throws CaptchaException 异常
     */
    void genAndRender(String captchaId, OutputStream os) throws CaptchaException;

    /**
     * 输出随机验证码图片流,并返回验证码值
     * @param captchaId 验证码标识
     * @param os 输出流
     * @param width 图片宽度
     * @param height 图片高度
     * @throws CaptchaException 异常
     */
    void genAndRender(String captchaId, OutputStream os, int width, int height) throws CaptchaException;

}

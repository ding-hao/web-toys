package cn.zhxu.toys.cache;

import cn.zhxu.xjson.JsonKit;
import cn.zhxu.data.TypeRef;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Troy.Zhou
 * @since v0.4.9
 */
public abstract class AbstractCacheService implements CacheService {

	private boolean notConvertStrToJson = false;

	@Override
	public <T> T cache(String key, Class<T> resType) {
		return cache(key, 0, resType, null);
	}

	@Override
	public <T> T cache(String key, TypeRef<T> resType) {
		return cache(key, 0, resType, null);
	}
	
	@Override
	public <T> T cacheShort(String key, Class<T> resType, CacheGetter<T> getter) {
		return cache(key, SHORT_CACHE_SECONDS, resType, getter);
	}

	@Override
	public <T> T cacheShort(String key, TypeRef<T> resType, CacheGetter<T> getter) {
		return cache(key, SHORT_CACHE_SECONDS, resType, getter);
	}
	
	@Override
	public <T> T cacheMedium(String key, Class<T> resType, CacheGetter<T> getter) {
		return cache(key, MEDIUM_CACHE_SECONDS, resType, getter);
	}
	
	@Override
	public <T> T cacheMedium(String key, TypeRef<T> resType, CacheGetter<T> getter) {
		return cache(key, MEDIUM_CACHE_SECONDS, resType, getter);
	}

	@Override
	public <T> T cacheLong(String key, Class<T> resType, CacheGetter<T> getter) {
		return cache(key, LONG_CACHE_SECONDS, resType, getter);
	}

	@Override
	public <T> T cacheLong(String key, TypeRef<T> resType, CacheGetter<T> getter) {
		return cache(key, LONG_CACHE_SECONDS, resType, getter);
	}
	
	@Override
	public void cacheShort(String key, Object object) {
		cache(key, SHORT_CACHE_SECONDS, object);
	}

	@Override
	public void cacheMedium(String key, Object object) {
		cache(key, MEDIUM_CACHE_SECONDS, object);
	}

	@Override
	public void cacheLong(String key, Object object) {
		cache(key, LONG_CACHE_SECONDS, object);
	}

	@Override
	public <T> T cache(String key, int timeoutSeconds, Class<T> resType, CacheGetter<T> getter) {
		return doCache(key, timeoutSeconds, resType, getter);
	}
	
	@Override
	public <T> T cache(String key, int timeoutSeconds, TypeRef<T> resType, CacheGetter<T> getter) {
		return doCache(key, timeoutSeconds, resType.getType(), getter);
	}
	
	@Override
	public <T> T cacheBean(String key, int timeoutSeconds, Class<T> resType, CacheGetter<T> getter) {
		return doCache(key, timeoutSeconds, resType, getter);
	}

	@Override
	public <T> List<T> cacheList(String key, int timeoutSeconds, Class<T> resType, CacheGetter<List<T>> getter) {
		return doCacheList(key, timeoutSeconds, resType, getter);
	}

	/**
	 * 先读，都读到则返回，否则若 getter 非空，则写
	 * @param <T> 泛型
	 * @param key 缓存键
	 * @param timeoutSeconds 过期秒数
	 * @param resType 返回类型
	 * @param getter 获取器
	 * @return 缓存值
	 */
	protected abstract <T> T doCache(String key, int timeoutSeconds, Type resType, CacheGetter<T> getter);
	
	/**
	 * 先读，都读到则返回，否则若 getter 非空，则写
	 * @param <T> 泛型
	 * @param key 缓存键
	 * @param timeoutSeconds 过期秒数
	 * @param resType 返回类型
	 * @param getter 获取器
	 * @return 缓存值
	 */
	protected abstract <T> List<T> doCacheList(String key, int timeoutSeconds, Class<T> resType, CacheGetter<List<T>> getter);
	
	@SuppressWarnings("all")
	protected <T> T toBean(String cache, Type resType) {
		if (NULL.equals(cache)) {
		    return null;
		}
		if (notConvertStrToJson && resType == String.class) {
			return (T) cache;
		}
		return JsonKit.toBean(resType, cache);
	}

	protected <T> List<T> toList(String cache, Class<T> resType) {
		if (NULL.equals(cache)) {
		    return new ArrayList<>();
		}
		return JsonKit.toList(resType, cache);
	}
	
    protected String toString(Object object) {
		if (notConvertStrToJson && object instanceof String) {
			return (String) object;
		}
    	if (object != null) {
			return JsonKit.toJson(object);
    	}
    	return NULL;
    }

	/**
	 * @return 是否不将字符串转换为 JSON
	 */
	public boolean isNotConvertStrToJson() {
		return notConvertStrToJson;
	}

	public void setNotConvertStrToJson(boolean notConvertStrToJson) {
		this.notConvertStrToJson = notConvertStrToJson;
	}

}

package cn.zhxu.toys.cache;

import cn.zhxu.data.TypeRef;

import java.util.List;

public interface CacheService {

    String NULL = "null";
    int LONG_CACHE_SECONDS = 3 * 24 * 3600;
    int MEDIUM_CACHE_SECONDS = 6 * 3600;
    int SHORT_CACHE_SECONDS = 3600;

	/**
     * 删除缓存
     * @param key 键
     */
    void delete(String key);
    
	 /**
      * 只读
      * @param key 键
      * @param resType 类型
      * @param <T> 泛型
      */
    <T> T cache(String key, Class<T> resType);

    /**
     * 只读
     * @param key 键
     * @param resType 类型
     * @param <T> 泛型
     */
    <T> T cache(String key, TypeRef<T> resType);
    
    /**
     * 先读，读不到则写
     * @param key 键
     * @param resType 类型
     * @param getter CacheGetter
     * @param <T> 泛型
     */
    <T> T cacheShort(String key, Class<T> resType, CacheGetter<T> getter);
    
    /**
     * 先读，读不到则写
     * @param key 键
     * @param resType 类型
     * @param getter CacheGetter
     * @param <T> 泛型
     */
    <T> T cacheShort(String key, TypeRef<T> resType, CacheGetter<T> getter);
    
    /**
     * 先读，读不到则写
     * @param key 键
     * @param resType 类型
     * @param getter CacheGetter
     * @param <T> 泛型
     */
    <T> T cacheMedium(String key, Class<T> resType, CacheGetter<T> getter);

    /**
     * 先读，读不到则写
     * @param key 键
     * @param resType 类型
     * @param getter CacheGetter
     * @param <T> 泛型
     */
    <T> T cacheMedium(String key, TypeRef<T> resType, CacheGetter<T> getter);
    
    /**
     * 先读，读不到则写
     * @param key 键
     * @param resType 类型
     * @param getter CacheGetter
     * @param <T> 泛型
     */
    <T> T cacheLong(String key, Class<T> resType, CacheGetter<T> getter);

    /**
     * 先读，读不到则写
     * @param key 键
     * @param resType 类型
     * @param getter CacheGetter
     * @param <T> 泛型
     */
    <T> T cacheLong(String key, TypeRef<T> resType, CacheGetter<T> getter);
    
    /**
     * 只写
     * @param key 键
     * @param object 对象
     */
    void cacheShort(String key, Object object);

    /**
     * 只写
     * @param key 键
     * @param object 对象
     */
    void cacheMedium(String key, Object object);

    /**
     * 只写
     * @param key 键
     * @param object 对象
     */
    void cacheLong(String key, Object object);

    /**
     * 只写
     * @param key 键
     * @param timeoutSeconds 过期秒数
     * @param object 对象
     */
    void cache(String key, int timeoutSeconds, Object object);

    /**
     * 先读，读不到则写
     * @param key 键
     * @param timeoutSeconds 过期秒数
     * @param resType 类型
     * @param getter CacheGetter
     * @param <T> 泛型
     */
    <T> T cache(String key, int timeoutSeconds, Class<T> resType, CacheGetter<T> getter);
    
    /**
     * 先读，读不到则写
     * @param key 键
     * @param timeoutSeconds 过期秒数
     * @param resType 类型
     * @param getter CacheGetter
     * @param <T> 泛型
     */
    <T> T cacheBean(String key, int timeoutSeconds, Class<T> resType, CacheGetter<T> getter);
    
    /**
     * 先读，读不到则写
     * @param key 键
     * @param timeoutSeconds 过期秒数
     * @param resType 类型
     * @param getter CacheGetter
     * @param <T> 泛型
     */
    <T> List<T> cacheList(String key, int timeoutSeconds, Class<T> resType, CacheGetter<List<T>> getter);
    
    /**
     * 先读，读不到则写
     * @param key 键
     * @param timeoutSeconds 过期秒数
     * @param resType 类型
     * @param getter CacheGetter
     * @param <T> 泛型
     */
    <T> T cache(String key, int timeoutSeconds, TypeRef<T> resType, CacheGetter<T> getter);
	
}

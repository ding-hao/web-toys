package cn.zhxu.toys.cache;

import org.springframework.data.redis.core.StringRedisTemplate;

import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author Troy
 *
 * @since 0.4.6
 */
public class RedisTemplateCacheService extends AbstractCacheService {

	
	private StringRedisTemplate redisTemplate;
	
	
	public RedisTemplateCacheService() { }
	
	public RedisTemplateCacheService(StringRedisTemplate redisTemplate) {
		this.redisTemplate = redisTemplate;
	}
	

	@Override
	public void delete(String key) {
		redisTemplate.delete(key);
	}
	
	@Override
	public void cache(String key, int timeoutSeconds, Object object) {
		redisTemplate.opsForValue().set(key, toString(object), timeoutSeconds, TimeUnit.SECONDS);
	}


	protected <T> T doCache(String key, int timeoutSeconds, Type resType, CacheGetter<T> getter) {
        String cache = redisTemplate.opsForValue().get(key);
        if (cache != null) {
            return toBean(cache, resType);
        }
        if (timeoutSeconds > 0 && getter != null) {
        	// 获取Object可能会阻塞一段时间，所以得提前释放Jedis连接，以免在高并发下造成连接耗尽问题
            T object = getter.doGet();
            cache(key, timeoutSeconds, object);
            return object;
        }
        return null;
	}
	
	protected <T> List<T> doCacheList(String key, int timeoutSeconds, Class<T> resType, CacheGetter<List<T>> getter) {
        String cache = redisTemplate.opsForValue().get(key);
        if (cache != null) {
            return toList(cache, resType);
        }
        if (timeoutSeconds > 0 && getter != null) {
        	// 获取Object可能会阻塞一段时间，所以得提前释放Jedis连接，以免在高并发下造成连接耗尽问题
            List<T> list = getter.doGet();
            cache(key, timeoutSeconds, list);
            return list;
        }
        return null;
	}

	public void setRedisTemplate(StringRedisTemplate redisTemplate) {
		this.redisTemplate = redisTemplate;
	}
	
	public StringRedisTemplate getRedisTemplate() {
		return redisTemplate;
	}
	
}

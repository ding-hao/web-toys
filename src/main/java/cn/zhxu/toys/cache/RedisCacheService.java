package cn.zhxu.toys.cache;

import java.lang.reflect.Type;
import java.util.List;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * CacheService Jedis 实现
 */
public class RedisCacheService extends AbstractCacheService {

	
	private JedisPool jedisPool;
	
	
	public RedisCacheService() { }
	
	public RedisCacheService(JedisPool jedisPool) {
		this.jedisPool = jedisPool;
	}

	@Override
	public void delete(String key) {
		try (Jedis jedis = jedisPool.getResource()) {
			jedis.del(key);
		}
	}

	@Override
	public void cache(String key, int timeoutSeconds, Object object) {
		try (Jedis jedis = jedisPool.getResource()) {
			jedis.setex(key, timeoutSeconds, toString(object));
		}
	}
	
	protected <T> T doCache(String key, int timeoutSeconds, Type resType, CacheGetter<T> getter) {
		try (Jedis jedis = jedisPool.getResource()) {
			String cache = jedis.get(key);
			if (cache != null) {
				return toBean(cache, resType);
			}
		}
        if (timeoutSeconds > 0 && getter != null) {
        	// 获取Object可能会阻塞一段时间，所以得提前释放Jedis连接，以免在高并发下造成连接耗尽问题
            T object = getter.doGet();
            cache(key, timeoutSeconds, object);
            return object;
        }
        return null;
	}
	
	protected <T> List<T> doCacheList(String key, int timeoutSeconds, Class<T> resType, CacheGetter<List<T>> getter) {
		try (Jedis jedis = jedisPool.getResource()) {
			String cache = jedis.get(key);
			if (cache != null) {
				return toList(cache, resType);
			}
		}
        if (timeoutSeconds > 0 && getter != null) {
        	// 获取Object可能会阻塞一段时间，所以得提前释放Jedis连接，以免在高并发下造成连接耗尽问题
            List<T> list = getter.doGet();
            cache(key, timeoutSeconds, list);
            return list;
        }
        return null;
	}
	
	public JedisPool getJedisPool() {
		return jedisPool;
	}

	public void setJedisPool(JedisPool jedisPool) {
		this.jedisPool = jedisPool;
	}
	
}

package cn.zhxu.toys.cache;

public interface CacheGetter<T> {

	
	T doGet();
	
	
}
